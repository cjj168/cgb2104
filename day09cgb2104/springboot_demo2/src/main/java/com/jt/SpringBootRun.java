package com.jt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//SpringBoot为了简化代码 提供了包扫描的机制,为包路径下的接口创建代理对象,之后交给
//Spring容器管理  可以在任意位置依赖注入
@MapperScan("com.jt.mapper")
public class SpringBootRun {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRun.class,args);
    }
}
