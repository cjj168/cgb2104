package com.jt.test;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis {

    //明确:注入一定是对象
    //SpringBoot为了整合mybatis,简化代码结构 Spring动态的为Mybatis的接口
    //创建代理对象
    //代理: 根据原有对象的模型,在运行期动态创建了一个一模一样功能的实例化对象
    //案例: 孙悟空(接口)/克隆一个一模一样的对象
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testFind(){
        System.out.println(userMapper.getClass());
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
    }

    //根据ID查询数据
    @Test
    public void findUserById(){

       User user = userMapper.findUserById(11);
       System.out.println(user);
    }

    //新增用户
    //数据从哪里来 前端动态获取
    @Test
    public void insert(){
        User user = new User();
        user.setName("星期五").setSex("男").setAge(18);
        userMapper.insert(user);
        System.out.println("新增成功");
    }
    //将星期五的年龄改为20 性别改为女
    @Test
    public void update(){
        User user = new User();
        user.setAge(20).setSex("女").setName("星期五");
        userMapper.updateByName(user);
    }

}
