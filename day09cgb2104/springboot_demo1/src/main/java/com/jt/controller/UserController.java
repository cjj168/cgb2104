package com.jt.controller;

import com.jt.service.UserService;
import com.jt.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@Component  //IOC 早期注解都叫@Component
//@Controller //Controller层中使用
//@Service    //Service层中使用
//@Repository //持久层使用的注解 一般不用 后期使用Mybatis代替
public class UserController {

    //1.面向接口编程
    @Autowired  //依赖注入
    private UserService userService;

    @RequestMapping("/getMsg")
    public String getMsg(){

        return userService.getMsg();
    }
}
