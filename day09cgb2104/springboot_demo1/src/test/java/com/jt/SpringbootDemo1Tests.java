package com.jt;

import com.jt.controller.UserController;
import com.jt.service.UserService;
import com.jt.service.UserServiceImpl;
import org.apache.catalina.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
//概括: 启动时Spring容器启动,启动之后可以自动的注入spring容器中的任意对象,进行单元测试
class SpringbootDemo1Tests {

    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;

    @Test
    void test01() {
        //UserController userController = new UserController();  对比
        String msg = userController.getMsg();
        System.out.println(msg);
    }
}
